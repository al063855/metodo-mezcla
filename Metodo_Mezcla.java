package metodo_mezcla;

/**
 *
 * @author hitzu
 */
import java.util.Arrays;
public class Metodo_Mezcla {

    static void mezclar(int arr[], int izquierda[], int derecha[]) {
		int indiceIzq=0, indiceDer=0, indiceArr = 0;
		
		
		while (indiceIzq < izquierda.length && indiceDer < derecha.length) { 
			if (izquierda[indiceIzq] < derecha[indiceDer]) { 
				arr[indiceArr] = izquierda[indiceIzq]; 
				indiceIzq+=1;
			}
			else { 
				arr[indiceArr] = derecha[indiceDer];
				indiceDer+=1;
			}
			indiceArr+=1;
		}
		
		
		while (indiceIzq < izquierda.length) {
			arr[indiceArr] = izquierda[indiceIzq];
			indiceIzq+=1;
			indiceArr+=1;
		}
		  
		while (indiceDer < derecha.length) { 
			arr[indiceArr] = derecha[indiceDer]; 
			indiceDer+=1;
			indiceArr+=1;
		}
	}
	
	static void ordenar(int arr[]) {
		if (arr.length >1) {
			int mitad = arr.length/2;
			int n1 = mitad;
			int n2 = arr.length - mitad;

			
			int izquierda[] = new int[n1];
			int derecha[] = new int[n2];

			
			for (int indiceIzq = 0; indiceIzq < n1; ++indiceIzq)
				izquierda[indiceIzq] = arr[indiceIzq];
			for (int indiceDer = 0; indiceDer < n2; ++indiceDer)
				derecha[indiceDer] = arr[mitad + indiceDer];
			
			ordenar(izquierda); 
			ordenar(derecha); 
			mezclar(arr, izquierda, derecha); 
		}
        }
    public static void main(String[] args) {
        // TODO code application logic here
        int datos[] = new int[] { 50, 6, 37, 92, 12, 4, 74};
		System.out.println("Arreglo original: " + Arrays.toString(datos));
		ordenar(datos);
		System.out.println("Arreglo ordenado: " + Arrays.toString(datos));
    }
    
}
